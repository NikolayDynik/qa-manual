package com.google;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import java.awt.*;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class FirstTest extends Settings {

    @Test
    public void firstTest() throws InterruptedException, AWTException {
        driver.get("https://www.google.com");
        driver.findElement(By.id("lst-ib")).sendKeys("todo mvc", Keys.ENTER);

        driver.findElement(By.linkText("TodoMVC")).sendKeys(Keys.ENTER);

        driver.findElement(By.linkText("Backbone.js")).sendKeys(Keys.ENTER);

        for (int i = 1; i <= 5; i++) {
            driver.findElement(By.className("new-todo")).sendKeys("Task " + i, Keys.ENTER);
        }
        Thread.sleep(1000);

        List<WebElement> label = driver.findElements(By.className("view"));
        int size = label.size();
        System.out.println("Items size " + size);
        Thread.sleep(1000);

        String itemsLeft = driver.findElement(By.xpath("//span[@class='todo-count']/strong")).getText();
        boolean isContain = itemsLeft.contains("5");
        System.out.println("Items size == itemsLeft? " + isContain);
        Thread.sleep(1000);


        driver.findElement(By.className("toggle")).click();
        Thread.sleep(1000);

        driver.findElement(By.className("destroy")).click();
        Thread.sleep(1000);

        driver.findElement(By.className("toggle")).click();
        Thread.sleep(1000);

        driver.findElement(By.className("clear-completed")).click();
        Thread.sleep(1000);

        driver.findElement(By.className("toggle")).click();
        Thread.sleep(1000);

        //Check Active tab

        driver.findElement(By.partialLinkText("Active")).sendKeys(Keys.ENTER);
        String itemsLeftActive = driver.findElement(By.xpath("//span[@class='todo-count']/strong")).getText();
        boolean isContainActive = itemsLeftActive.contains("2");
        System.out.println("Items in active tab == items size? " + isContainActive);
        Thread.sleep(1000);

        //Check completed tasks
        driver.findElement(By.partialLinkText("Completed")).click();
        Thread.sleep(1000);
        String itemsLeftComplete = driver.findElement(By.xpath("//span[@class='todo-count']/strong")).getText();
        boolean isContainComplete = itemsLeftComplete.contains("1");
        System.out.println("Items in complete tab == items size? " + isContainActive);
        Thread.sleep(1000);

        driver.findElement(By.className("clear-completed")).click();
        Thread.sleep(1000);
    }

}

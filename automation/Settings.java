package com.google;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Settings {

    WebDriver driver;


    @BeforeClass
    public static void mainPrecondition() {
        System.setProperty("webdriver.gecko.driver", "D:\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
    }

    @Before
    public void preconditions() {
        driver = new ChromeDriver();
    }

    @After
    public void postCondition() {
        driver.quit();
    }
}
